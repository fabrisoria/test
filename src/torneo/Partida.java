package torneo;
import java.util.*;

/**
 * 
 */
public class Partida {

    /**
     * Default constructor
     */
    public Partida() {
    }

    /**
     * 
     */
    public int id;

    /**
     * 
     */
    public int equipo1;

    /**
     * 
     */
    public int equipo2;

    /**
     * 
     */
    public int torneo;

    /**
     * 
     */
    public Date fecha;

    /**
     * 
     */
    public int resultado;

}